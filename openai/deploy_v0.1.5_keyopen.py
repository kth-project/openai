import openai
import os
import re

class KubernetesYAMLCreator:
    def __init__(self):
        # 환경변수에서 OpenAI API 키를 불러옵니다.
        self.openai_api_key = os.getenv("OPENAI_API_KEY")
        openai.api_key = self.openai_api_key

    def create_query(self, kind, options=None):
        # YAML 파일 생성 쿼리를 설정하고, 설정 값을 포함하여 API 쿼리 스트링을 반환합니다.
        if options:
            options_list = []
            # 옵션을 처리하여 옵션 목록으로 변환합니다.
            for k, v in options.items():
                # 특정 옵션에 대한 처리입니다.
                if k == "deploymentName":
                    #... 기타 특정 옵션 처리
                    options_list.append(f"name={v}")
                elif k == "containerName":
                    options_list.append(f"name (container)={v}")
                elif k == "matchLabels":
                    v = "{" + ", ".join([f"{key}: {value}" for key, value in v.items()]) + "}"
                    options_list.append(f"{k}={v}")
                elif k == "ingressName":
                    options_list.append(f"name (ingress)={v}")
                elif k == "path":
                    options_list.append(f"path={v}")
                elif k == "pathType":
                    options_list.append(f"pathType={v}")
                elif k == "serviceName":
                    options_list.append(f"name (service)={v}")
                elif k == "portName":
                    options_list.append(f"name (port)={v}")
                else:
                    options_list.append(f"{k}={v}")

            options_string = ", ".join(options_list)
            return f"Please write a Kubernetes {kind} YAML file with the following options: {options_string}."
        else:
            return f"Please write a Kubernetes {kind} YAML file for me."

    def get_chat_response(self, prompt):
        # 여기서 입력된 prompt 값을 출력합니다.
        print(f"Prompt 입력 값: {prompt}")
        # OpenAI API를 사용하여 챗봇 응답을 생성하고 가져옵니다.
        chat_response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": prompt}],
            temperature=0.3,
            n=1
        )
        return chat_response.choices[0].message.content
        
    def extract_yaml(self, content):
        # YAML 형식의 응답에서 YAML 텍스트를 추출합니다.
        match = re.search(r"```\n(.*?)```", content, re.DOTALL)
        if match:
            return match.group(1).rstrip()
        else:
            return None        
        
    def save_yaml_file(self, yaml_text, file_name):
        # YAML 텍스트를 파일로 저장합니다. 
        with open(file_name, "w") as f:
            f.write(yaml_text)

    def get_deployment_options(self):
        # 배포 옵션을 가져와 사용자로부터 입력을 받습니다. 
        #... 기타 옵션 처리 및 입력
        # 여러 리소스 유형에 대한 옵션 얻기 함수 (get_ingress_options, get_service_options 등)
        options = {}

        deployment_name = input("Enter name for deployment (Press Enter for default): ")
        if deployment_name:
            options["deploymentName"] = deployment_name


        deployment_namespace = input("Enter name for deployment namespace (Press Enter for default): ")
        if deployment_namespace:
            options["deploymentNameSpace"] = deployment_namespace

        replicas = input("Enter replicas for deployment (Press Enter for default): ")
        if replicas:
            options["replicas"] = replicas

        container_name = input("Enter name for the container (Press Enter for default): ")
        if container_name:
            options["containerName"] = container_name

        image = input("Enter container image: ")
        if image:
            options["image"] = image
            
        container_port = input("Enter container port (Press Enter for default): ")
        if container_port:
            options["containerPort"] = container_port

        match_labels_input = input("Enter matchLabels for selector as key:value pairs separated by commas (e.g., app:myapp,version:v1): ")
        if match_labels_input:
            match_labels = {}
            for pair in match_labels_input.split(','):
                key, value = pair.split(':')
                match_labels[key.strip()] = value.strip()
            options["matchLabels"] = match_labels


        # Liveness Probe
        liveness_probe_enabled = input("Would you like to add a livenessProbe? (y/n): ")
        if liveness_probe_enabled.lower() == "y":
            liveness_probe_options = {}
            liveness_probe_type = input("Enter livenessProbe type (httpGet, tcpSocket, or exec): ")
            liveness_probe_options["type"] = liveness_probe_type.strip()
            if liveness_probe_type == "httpGet":
                http_get_options = {}
                http_get_options["path"] = input("Enter httpGet path (e.g., /health): ")
                http_get_options["port"] = int(input("Enter httpGet port: "))
                liveness_probe_options["httpGet"] = http_get_options
            elif liveness_probe_type == "tcpSocket":
                tcp_socket_options = {}
                tcp_socket_options["port"] = int(input("Enter tcpSocket port: "))
                liveness_probe_options["tcpSocket"] = tcp_socket_options
            elif liveness_probe_type == "exec":
                exec_options = {}
                command = input("Enter exec command (e.g., 'ls /tmp'): ")
                exec_options["command"] = command.split()
                liveness_probe_options["exec"] = exec_options
                
            options["livenessProbe"] = liveness_probe_options

        # Readiness Probe
        readiness_probe_enabled = input("Would you like to add a readinessProbe? (y/n): ")
        if readiness_probe_enabled.lower() == "y":
            readiness_probe_options = {}
            readiness_probe_type = input("Enter readinessProbe type (httpGet, tcpSocket, or exec): ")
            readiness_probe_options["type"] = readiness_probe_type.strip()
            if readiness_probe_type == "httpGet":
                http_get_options = {}
                http_get_options["path"] = input("Enter readiness httpGet path (e.g., /health): ")
                http_get_options["port"] = int(input("Enter readiness httpGet port: "))
                readiness_probe_options["httpGet"] = http_get_options
            elif readiness_probe_type == "tcpSocket":
                tcp_socket_options = {}
                tcp_socket_options["port"] = int(input("Enter readiness tcpSocket port: "))
                readiness_probe_options["tcpSocket"] = tcp_socket_options
            elif readiness_probe_type == "exec":
                exec_options = {}
                command = input("Enter readiness exec command (e.g., 'ls /tmp'): ")
                exec_options["command"] = command.split()
                readiness_probe_options["exec"] = exec_options

            options["readinessProbe"] = readiness_probe_options

        startup_probe_enabled_input = input("Do you want to enable the startup probe for the container? (y/n, default is no): ")
        if startup_probe_enabled_input.lower() == 'y':
            options["startupProbe"] = {
                "failureThreshold": int(input("Enter the failure threshold for the startup probe (number, default is 3): ")),
                "periodSeconds": int(input("Enter the period between probe attempts in seconds (number, default is 10): ")),
                "timeoutSeconds": int(input("Enter the probe timeout in seconds (number, default is 1): ")),
                "initialDelaySeconds": int(input("Enter the time before the first probe (in seconds) after the container started (initial delay, default is 0): ")),
            }

        # PostStart 
        lifecycle_hooks_enabled = input("Would you like to add PostStart and/or PreStop hooks? (y/n): ")
        if lifecycle_hooks_enabled.lower() == "y":
            lifecycle_hooks = {}
            
            post_start_enabled = input("Would you like to add a PostStart hook? (y/n): ")
            if post_start_enabled.lower() == "y":
                post_start_options = {}
                post_start_type = input("Enter PostStart hook type (exec, httpGet, or tcpSocket): ")
                post_start_options["type"] = post_start_type.strip()

                if post_start_type == "exec":
                    command = input("Enter PostStart exec command (e.g., 'touch /tmp/post_start'): ")
                    post_start_options["command"] = command.split()
                elif post_start_type == "httpGet":
                    http_get_options = {}
                    http_get_options["path"] = input("Enter PostStart httpGet path (e.g., /post_start): ")
                    http_get_options["port"] = int(input("Enter PostStart httpGet port: "))
                    post_start_options["httpGet"] = http_get_options
                elif post_start_type == "tcpSocket":
                    tcp_socket_options = {}
                    tcp_socket_options["port"] = int(input("Enter PostStart tcpSocket port: "))
                    post_start_options["tcpSocket"] = tcp_socket_options

                lifecycle_hooks["postStart"] = post_start_options

            # PreStop
            pre_stop_enabled = input("Would you like to add a PreStop hook? (y/n): ")
            if pre_stop_enabled.lower() == "y":
                pre_stop_options = {}
                pre_stop_type = input("Enter PreStop hook type (exec, httpGet, or tcpSocket): ")
                pre_stop_options["type"] = pre_stop_type.strip()

                if pre_stop_type == "exec":
                    command = input("Enter PreStop exec command (e.g., 'touch /tmp/pre_stop'): ")
                    pre_stop_options["command"] = command.split()
                elif pre_stop_type == "httpGet":
                    http_get_options = {}
                    http_get_options["path"] = input("Enter PreStop httpGet path (e.g., /pre_stop): ")
                    http_get_options["port"] = int(input("Enter PreStop httpGet port: "))
                    pre_stop_options["httpGet"] = http_get_options
                elif pre_stop_type == "tcpSocket":
                    tcp_socket_options = {}
                    tcp_socket_options["port"] = int(input("Enter PreStop tcpSocket port: "))
                    pre_stop_options["tcpSocket"] = tcp_socket_options

                lifecycle_hooks["preStop"] = pre_stop_options

            options["lifecycleHooks"] = lifecycle_hooks

        return options

    def get_ingress_options(self):  
        options = {}

        ingress_name = input("Enter name for ingress (Press Enter for default): ")
        if ingress_name:
            options["ingressName"] = ingress_name

        ingress_namespace = input("Enter name for ingress namespace (Press Enter for default): ")
        if ingress_namespace:
            options["ingressNameSpace"] = ingress_namespace

        path = input("Enter path for ingress (e.g. /api): ")
        if path:
            options["path"] = path

        path_type = input("Enter path type for ingress (e.g. Prefix, Exact): ")
        if path_type:
            options["pathType"] = path_type

        service_name = input("Enter service name for ingress (Press Enter for default): ")
        if service_name:
            options["serviceName"] = service_name

        port_name = input("Enter port name for the service in ingress (Press Enter for default): ")
        if port_name:
            options["portName"] = port_name

        host = input("Enter host for ingress (e.g. example.com): ")
        if host:
            options["host"] = host

        use_tls = input("Do you want to use TLS? (y/n): ")
        if use_tls.lower() == 'y':
            options["useTLS"] = True
            tls_secret_name = input("Enter TLS secret name: ")
            options["tlsSecretName"] = tls_secret_name
        else:
            options["useTLS"] = False

        ingress_class = input("Enter ingress controller class (e.g. nginx, traefik): ")
        if ingress_class:
            options["ingressClass"] = ingress_class

        return options
    
    def get_service_options(self):  
        options = {}

        service_name = input("Enter name for service (Press Enter for default): ")
        if service_name:
            options["serviceName"] = service_name
            
        service_labels_input = input("Enter service labels as key:value pairs separated by commas (e.g., app:myapp,version:v1): ")
        if service_labels_input:
            service_labels = {}
            for pair in service_labels_input.split(','):
                key, value = pair.split(':')
                service_labels[key.strip()] = value.strip()
            options["serviceLabels"] = service_labels

        selector_input = input("Enter selector as key:value pairs separated by commas (e.g., app:myapp,version:v1): ")
        if selector_input:
            selector = {}
            for pair in selector_input.split(','):
                key, value = pair.split(':')
                selector[key.strip()] = value.strip()
            options["selector"] = selector

        port_name = input("Enter port name for the service (Press Enter for default): ")
        if port_name:
            options["portName"] = port_name

        port = input("Enter port number for the service: ")
        if port:
            options["port"] = port

        target_port = input("Enter targetPort for the service (Press Enter for default): ")
        if target_port:
            options["targetPort"] = target_port

        service_type_input = input("Enter service type (ClusterIP, NodePort, LoadBalancer, ExternalName) or press Enter for default: ")
        valid_types = ["ClusterIP", "NodePort", "LoadBalancer", "ExternalName"]
        if service_type_input:
            if service_type_input in valid_types:
                options["serviceType"] = service_type_input
            else:
                print("Invalid service type. Using default.")

        return options
        
    def get_storage_class_options(self):
        options = {}

        storage_class_name = input("Enter the StorageClass name (required): ")
        options["storageClassName"] = storage_class_name

        provisioner = input("Enter the provisioner for the StorageClass (required, e.g., kubernetes.io/aws-ebs for AWS EBS): ")
        options["provisioner"] = provisioner

        reclaim_policy = input("Enter the reclaim policy for the StorageClass (Delete or Retain, default is Delete): ")
        if reclaim_policy:
            options["reclaimPolicy"] = reclaim_policy

        allow_volume_expansion = input("Allow volume expansion for the StorageClass? (y/n, default is no): ")
        if allow_volume_expansion.lower() == 'y':
            options["allowVolumeExpansion"] = True
        else:
            options["allowVolumeExpansion"] = False

        volume_binding_mode = input("Enter the volume binding mode for the StorageClass (Immediate or WaitForFirstConsumer, default is Immediate): ")
        if volume_binding_mode:
            options["volumeBindingMode"] = volume_binding_mode

        parameters_input = input("Do you want to specify additional parameters for the StorageClass? (y/n): ")
        if parameters_input.lower() == 'y':
            parameters = {}
            while True:
                parameter_key = input("Enter the parameter key (or type 'done' to finish): ")
                if parameter_key.lower() == 'done':
                    break
                parameter_value = input(f"Enter the value for the '{parameter_key}': ")
                parameters[parameter_key] = parameter_value
            options["parameters"] = parameters

        return options

    def get_pv_options(self):  
        options = {}

        pv_name = input("Enter name for pv (Press Enter for default): ")
        if pv_name:
            options["pvName"] = pv_name

        storage = input("Enter storage capacity for the pv (e.g., 10Gi): ")
        if storage:
            options["storage"] = storage

        access_modes_input = input("Enter access modes separated by commas (e.g., ReadWriteOnce, ReadOnlyMany, ReadWriteMany): ")
        if access_modes_input:
            access_modes = [mode.strip() for mode in access_modes_input.split(',')]
            options["accessModes"] = access_modes

        reclaim_policy = input("Enter the persistent volume reclaim policy (Available options: Retain, Delete, Recycle): ")
        if reclaim_policy:
            options["persistentVolumeReclaimPolicy"] = reclaim_policy

        storage_class_name = input("Enter the name of the storage class (Press Enter for default): ")
        if storage_class_name:
            options["storageClassName"] = storage_class_name

        host_path = input("Enter the path on the host (e.g., /data/pv001/): ")
        if host_path:
            options["hostPath"] = {"path": host_path}
        return options

    def get_pvc_options(self):  
        options = {}

        pvc_name = input("Enter name for pvc (Press Enter for default): ")
        if pvc_name:
            options["pvcName"] = pvc_name

        pvc_namespace = input("Enter name for pvc namespace (Press Enter for default): ")
        if pvc_namespace:
            options["pvcNameSpace"] = pvc_namespace

        access_modes_input = input("Enter access modes separated by commas (e.g., ReadWriteOnce, ReadOnlyMany, ReadWriteMany): ")
        if access_modes_input:
            access_modes = [mode.strip() for mode in access_modes_input.split(',')]
            options["accessModes"] = access_modes

        storage_class_name = input("Enter the storage class name for the PVC (for cloud and on-premises environments): ")
        if storage_class_name:
            options["storageClassName"] = storage_class_name
        else:
            storage_class_name = None

        storage = input("Enter storage capacity for the pvc (e.g., 10Gi): ")
        if storage:
            options["storage"] = storage

        volume_mode = input("Enter volume mode for the PVC (Filesystem or Block, default is Filesystem): ")
        if volume_mode:
            options["volumeMode"] = volume_mode

        datasource_input = input("Do you want to specify a data source for the PVC? (y/n): ")
        if datasource_input.lower() == 'y':
            datasource_kind = input("Enter the data source kind (PersistentVolumeClaim or VolumeSnapshot): ")
            datasource_name = input("Enter the data source name: ")
            options["dataSource"] = {
                "kind": datasource_kind,
                "name": datasource_name
            }

        return options


    def get_hpa_options(self):  
        options = {}

        hpa_name = input("Enter name for HPA (Press Enter for default): ")
        if hpa_name:
            options["hpaName"] = hpa_name
            
        hpa_namespace = input("Enter name for HPA namespace (Press Enter for default): ")
        if hpa_namespace:
            options["hpaNameSpace"] = hpa_namespace

        scale_target_ref_name = input("Enter scaleTargetRef name (e.g., my-deployment): ")
        if scale_target_ref_name:
            options["scaleTargetRefName"] = scale_target_ref_name

        min_replicas = input("Enter the minimum number of replicas: ")
        if min_replicas:
            options["minReplicas"] = int(min_replicas)

        max_replicas = input("Enter the maximum number of replicas: ")
        if max_replicas:
            options["maxReplicas"] = int(max_replicas)

        target_cpu_utilization_percentage = input("Enter target CPU utilization percentage (e.g., 50): ")
        if target_cpu_utilization_percentage:
            options["targetCPUUtilizationPercentage"] = int(target_cpu_utilization_percentage)

        return options

    def run(self):
        # 리소스 유형인 '종류' 및 옵션 얻기 함수 맵 (get_deployment_options, get_ingress_options 등)
        input_number = int(input("Enter a number (1: Deployment, 2: Ingress, 3: Service, 4: StorageClass 5: PV, 6: PVC, 7: HPA): "))
        #... 기타 리소스 유형
        kind_map = {
            1: "deployment",
            2: "ingress",
            3: "service",
            4: "storageclass",
            5: "pv",
            6: "pvc",
            7: "HorizontalPodAutoscaler"
        }
        # 리소스 유형마다 해당 옵션을 가져오는 함수 매핑
        # ... 기타 옵션 가져오기 함수 매핑
        options_map = {
            1: self.get_deployment_options,
            2: self.get_ingress_options,
            3: self.get_service_options,
            4: self.get_storage_class_options,
            5: self.get_pv_options,
            6: self.get_pvc_options,
            7: self.get_hpa_options
        }
        # 사용자의 입력에 따라 리소스 유형(kind) 및 옵션(options)을 구합니다.
        kind = kind_map.get(input_number)

        options = {}
        # 사용자가 입력한 번호에 대한 옵션 가져오기 함수가 있는 경우 호출합니다.
        if kind:
            options = options_map.get(input_number)()
            # API 쿼리를 생성하고 GPT-3.5-turbo 모델을 사용하여 응답을 검색합니다.
            prompt = self.create_query(kind, options)  
            response = self.get_chat_response(prompt)  
            yaml_text = self.extract_yaml(response)  
            # YAML 텍스트를 실패한 경우와 성공한 경우에 따라 처리합니다.
            if yaml_text:
                print(yaml_text)

                file_name = f"{kind}_generated.yaml"
                self.save_yaml_file(yaml_text, file_name)  
                print(f"{file_name}에 저장되었습니다.")
            else:
                print("YAML 텍스트를 찾을 수 없습니다.") 
        else:
            print("올바른 숫자를 입력하세요.")

if __name__ == "__main__":
    # 프로그램을 실행합니다.
    creator = KubernetesYAMLCreator()  
    creator.run()

