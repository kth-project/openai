import openai
import os
import re

openai.api_key = os.getenv("OPENAI_API_KEY")

def create_query(kind, options=None):
    if options:
        options_string = ', '.join([f'{k}={v}' for k, v in options.items()])
        return f"Please write a Kubernetes {kind} YAML file with the following options: {options_string}."
    else:
        return f"Please write a Kubernetes {kind} YAML file for me."

def get_chat_response(prompt):
    chat_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": prompt}],
        temperature=0.3,
        n=1
    )
    return chat_response.choices[0].message.content

def extract_yaml(content):
    match = re.search(r"```\n(.*?)```", content, re.DOTALL)
    if match:
        return match.group(1).rstrip()
    else:
        return None

def save_yaml_file(yaml_text, file_name):
    with open(file_name, "w") as f:
        f.write(yaml_text)

def get_deployment_options():
    options = {}

    replicas = input("Enter replicas for deployment (Press Enter for default): ")
    if replicas:
        options["replicas"] = replicas

    image = input("Enter container image: ")
    if image:
        options["image"] = image
        
    container_port = input("Enter container port (Press Enter for default): ")
    if container_port:
        options["containerPort"] = container_port

    # 추가하실 다른 옵션을 이곳에 추가하세요.

    return options

input_number = int(input("Enter a number (1: Deployment, 2: Ingress, 3: Service, 4: PV, 5: PVC): "))

kind_map = {
    1: "deployment",
    2: "ingress",
    3: "service",
    4: "pv",
    5: "pvc",
}

kind = kind_map.get(input_number)
options = {}

if kind:
    if kind == "deployment":
        options = get_deployment_options()

    # 만약 다른 리소스 타입에 대해 옵션을 추가하려면 비슷한 함수를 만들고 여기에서 호출하세요.

    prompt = create_query(kind, options)
    response = get_chat_response(prompt)
    yaml_text = extract_yaml(response)
    if yaml_text:
        print(yaml_text)

        file_name = f"{kind}_generated.yaml"
        save_yaml_file(yaml_text, file_name)
        print(f"{file_name}에 저장되었습니다.")
    else:
        print("YAML 텍스트를 찾을 수 없습니다.") 
else:
    print("올바른 숫자를 입력하세요.")
