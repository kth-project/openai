# 1. openai api 를 사용한다.
# 2. python 을 사용한다.
# 3. openapi.ChatCompletion.create 사용한다.
# 4. input으로 사용자가 1~5번 입력 시 5가지의 kind 를 할당(deployment, ingress, service, pv, pvc)하는 코드를 생성한다. 
# 6. input 으로 받은 1~5번에 대한 값을 openai api 로 질의 하여 yaml 파일을 추출한다.
# 7. 추출한 yaml 파일에서 최대한 많이 사용자가 정의 할 수 있도록 if문을 사용하여 입력 받을 수 있도록 한다. 
# 8. 7번에 대한 예시로 kind가 deployment 라면 name, image, label, namespace 등이 있을 것이고, pv 의 경우 volume type 등 여러 가지의 설정을 정의 할 수 있는 것 처럼 한다.  
# 9. 정의가 완료된 yaml파일은 local에 저장 할 수 있도록 한다.

import openai
import os

# Set the API key
openai.api_key = os.getenv("OPENAI_API_KEY")

# Get the object kind based on user input
get_object_kind = {
    1: "Deployment",
    2: "Ingress",
    3: "Service",
    4: "PersistentVolume",
    5: "PersistentVolumeClaim",
}

def ask_gpt_4_template(object_kind, user_input):
    prompt = f"Create a customizable YAML template for a Kubernetes {object_kind} with user inputs: {user_input}"
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": prompt}],
        max_tokens=2000,
        n=1,
        stop=None,
        temperature=0.5,
    )
    return response.choices[0].message['content']

def create_and_save_yaml(kind):
    user_inputs = {
      "deployment": "name, image, label, namespace",
      "ingress": "name, namespace, rules, tls",
      "service": "name, namespace, type, selector",
      "pv": "name, storageClassName, capacity, accessModes, volumeMode",
      "pvc": "name, namespace, storageClassName, accessModes, resources"
    }
    template = ask_gpt_4_template(kind, user_inputs[kind.lower()])
    print(f"YAML Template for {kind}:")
    print(template)
    
    with open(f"{kind.lower()}.yaml", "w") as f:
        f.write(template)

user_input = int(input("Enter a number (1: Deployment, 2: Ingress, 3: Service, 4: PV, 5: PVC): "))

if 1 <= user_input <= 5:
    chosen_kind = get_object_kind[user_input]
    create_and_save_yaml(chosen_kind)
else:
    print("Invalid input. Please enter a number between 1 and 5.")
