# 1. openai api 를 사용한다.
# 2. python 을 사용한다.
# 3. openapi.ChatCompletion.create 사용한다.
# 4. input 으로 사용자가 1번을 선택하였을 때는 k8s kind를 deployment, 2번을 선택하였을 때는 kind를 ingress를 사용하는 파일을 작성 해달라고 요청한다. 
# 5. 각 kind를 1,2로 선택 완료 했다면 1번에 대한 deployment의 name을 사용자가 입력 할 수 있도록 한다. 
# 6. 응답한 값에 yaml 파일을 확인한다.
# 7. yaml 파일 만 추출한다.
# 8. 추출한 yaml 파일을 local에 반환 한다.

import openai
import os
import yaml

# OpenAI API 인증 정보를 설정합니다.
openai.api_key = "YOUR_API_KEY"

# 사용자에게 입력을 받습니다.
kind_input = int(input("1: 사용하려면 Deployment, 2: 사용하려면 Ingress를 선택하세요: "))
kind = "Deployment" if kind_input == 1 else "Ingress"

# 이름 입력
name = input(f"{kind}의 이름을 입력하세요: ")

# 대화 프롬프트를 정의합니다.
prompt = (
    "1. Using OpenAI API.\n"
    "2. Using Python.\n"
    "3. Using openai.ChatCompletion.create.\n"
    f"4. Write a YAML file with a k8s {kind.lower()} named {name}.\n"
    "5. Check the YAML file in the response.\n"
    "6. Extract the YAML file.\n"
    "7. Save the extracted YAML file locally."
)

# OpenAI의 Chat API를 사용하여 요청을 전달하고 응답을 받습니다.
response = openai.ChatCompletion.create(
    engine="gpt-4",
    messages=[{
        "role": "system",
        "content": prompt
    }]
)

# 응답에서 YAML 파일 부분을 찾고 추출합니다.
message = response["choices"][0]["message"]["content"]
index_start = message.index("---\n")
yaml_content = message[index_start:]

# 추출한 YAML 파일 부분을 지정된 이름으로 local에 저장합니다.
yaml_output_path = f"{kind.lower()}-{name}.yaml"
with open(yaml_output_path, 'w') as yaml_file:
    yaml_file.write(yaml_content)

print(f"{kind} YAML file has been saved at {yaml_output_path}")
