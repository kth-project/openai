# 1. openai api 를 사용한다.
# 2. python 을 사용한다.
# 3. openapi.ChatCompletion.create 사용한다.
# 4. openai api model은 gpt-3.5-turbo 를 사용한다.
# 5. input으로 사용자가 1~5번 입력 시 5가지의 kind 를 할당(deployment, ingress, service, pv, pvc)하는 코드를 생성한다. 
# 6. input 으로 받은 1~5번에 대한 값을 openai api 로 질의 한다.

import openai
import os
import re

openai.api_key = os.getenv("OPENAI_API_KEY")

def create_query(kind):
    return f"Please write a Kubernetes {kind} YAML file for me."
    
def get_chat_response(prompt):
    chat_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": prompt}],
    )
    return chat_response.choices[0].message.content


input_number = int(input("1~5번 사이의 숫자를 입력하세요: "))

kind_map = {
    1: "deployment",
    2: "ingress",
    3: "service",
    4: "pv",
    5: "pvc",
}

kind = kind_map.get(input_number)
if kind:
    prompt = create_query(kind)
    response = get_chat_response(prompt)
    print(response)
else:
    print("올바른 숫자를 입력하세요.")

