import openai
import os
import re

openai.api_key = os.getenv("OPENAI_API_KEY")

def create_query(kind, options=None):
    if options:
        options_list = []
        for k, v in options.items():
            if k == "deploymentName":
                options_list.append(f"name={v}")
            elif k == "containerName":
                options_list.append(f"name (container)={v}")
            elif k == "matchLabels":
                v = "{" + ", ".join([f"{key}: {value}" for key, value in v.items()]) + "}"
                options_list.append(f"{k}={v}")
            elif k == "ingressName":
                options_list.append(f"name (ingress)={v}")
            elif k == "path":
                options_list.append(f"path={v}")
            elif k == "pathType":
                options_list.append(f"pathType={v}")
            elif k == "serviceName":
                options_list.append(f"name (service)={v}")
            elif k == "portName":
                options_list.append(f"name (port)={v}")
            else:
                options_list.append(f"{k}={v}")

        options_string = ", ".join(options_list)
        return f"Please write a Kubernetes {kind} YAML file with the following options: {options_string}."
    else:
        return f"Please write a Kubernetes {kind} YAML file for me."
def get_chat_response(prompt):
    chat_response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": prompt}],
        temperature=0.3,
        n=1
    )
    return chat_response.choices[0].message.content

def extract_yaml(content):
    match = re.search(r"```\n(.*?)```", content, re.DOTALL)
    if match:
        return match.group(1).rstrip()
    else:
        return None

def save_yaml_file(yaml_text, file_name):
    with open(file_name, "w") as f:
        f.write(yaml_text)

def get_deployment_options():
    options = {}

    deployment_name = input("Enter name for deployment (Press Enter for default): ")
    if deployment_name:
        options["deploymentName"] = deployment_name

    replicas = input("Enter replicas for deployment (Press Enter for default): ")
    if replicas:
        options["replicas"] = replicas

    container_name = input("Enter name for the container (Press Enter for default): ")
    if container_name:
        options["containerName"] = container_name

    image = input("Enter container image: ")
    if image:
        options["image"] = image
        
    container_port = input("Enter container port (Press Enter for default): ")
    if container_port:
        options["containerPort"] = container_port

    # matchLabels 추가
    match_labels_input = input("Enter matchLabels for selector as key:value pairs separated by commas (e.g., app:myapp,version:v1): ")
    if match_labels_input:
        match_labels = {}
        for pair in match_labels_input.split(','):
            key, value = pair.split(':')
            match_labels[key.strip()] = value.strip()
        options["matchLabels"] = match_labels


    return options

def get_ingress_options():
    options = {}

    ingress_name = input("Enter name for ingress (Press Enter for default): ")
    if ingress_name:
        options["ingressName"] = ingress_name

    path = input("Enter path for ingress (e.g. /api): ")
    if path:
        options["path"] = path

    path_type = input("Enter path type for ingress (e.g. Prefix, Exact): ")
    if path_type:
        options["pathType"] = path_type

    service_name = input("Enter service name for ingress (Press Enter for default): ")
    if service_name:
        options["serviceName"] = service_name

    port_name = input("Enter port name for the service in ingress (Press Enter for default): ")
    if port_name:
        options["portName"] = port_name

    return options

def get_service_options():
    options = {}

    service_name = input("Enter name for service (Press Enter for default): ")
    if service_name:
        options["serviceName"] = service_name
    
    return options

def get_pv_options():
    options = {}

    pv_name = input("Enter name for pv (Press Enter for default): ")
    if pv_name:
        options["pvName"] = pv_name

    return options

def get_pvc_options():
    options = {}

    pvc_name = input("Enter name for pvc (Press Enter for default): ")
    if pvc_name:
        options["pvcName"] = pvc_name

    return options

input_number = int(input("Enter a number (1: Deployment, 2: Ingress, 3: Service, 4: PV, 5: PVC): "))

kind_map = {
    1: "deployment",
    2: "ingress",
    3: "service",
    4: "pv",
    5: "pvc",
}

options_map = {
    1: get_deployment_options,
    2: get_ingress_options,
    3: get_service_options,
    4: get_pv_options,
    5: get_pvc_options
}

kind = kind_map.get(input_number)
options = {}

if kind:
    options = options_map.get(input_number)()

    # 만약 다른 리소스 타입에 대해 옵션을 추가하려면 비슷한 함수를 만들고 여기에서 호출하세요.

    prompt = create_query(kind, options)
    response = get_chat_response(prompt)
    yaml_text = extract_yaml(response)
    if yaml_text:
        print(yaml_text)

        file_name = f"{kind}_generated.yaml"
        save_yaml_file(yaml_text, file_name)
        print(f"{file_name}에 저장되었습니다.")
    else:
        print("YAML 텍스트를 찾을 수 없습니다.") 
else:
    print("올바른 숫자를 입력하세요.")
