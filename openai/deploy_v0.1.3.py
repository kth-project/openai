import openai
import os
import re

class KubernetesYAMLCreator:
    def __init__(self):
        self.openai_api_key = os.getenv("OPENAI_API_KEY")
        openai.api_key = self.openai_api_key

    def create_query(self, kind, options=None):  
        if options:
            options_list = []
            for k, v in options.items():
                if k == "deploymentName":
                    options_list.append(f"name={v}")
                elif k == "containerName":
                    options_list.append(f"name (container)={v}")
                elif k == "matchLabels":
                    v = "{" + ", ".join([f"{key}: {value}" for key, value in v.items()]) + "}"
                    options_list.append(f"{k}={v}")
                elif k == "ingressName":
                    options_list.append(f"name (ingress)={v}")
                elif k == "path":
                    options_list.append(f"path={v}")
                elif k == "pathType":
                    options_list.append(f"pathType={v}")
                elif k == "serviceName":
                    options_list.append(f"name (service)={v}")
                elif k == "portName":
                    options_list.append(f"name (port)={v}")
                else:
                    options_list.append(f"{k}={v}")

            options_string = ", ".join(options_list)
            return f"Please write a Kubernetes {kind} YAML file with the following options: {options_string}."
        else:
            return f"Please write a Kubernetes {kind} YAML file for me."

    def get_chat_response(self, prompt):  
        chat_response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "system", "content": "You are a helpful assistant."}, {"role": "user", "content": prompt}],
            temperature=0.3,
            n=1
        )
        return chat_response.choices[0].message.content
        
    def extract_yaml(self, content):  
        match = re.search(r"```\n(.*?)```", content, re.DOTALL)
        if match:
            return match.group(1).rstrip()
        else:
            return None
        
    def save_yaml_file(self, yaml_text, file_name):  
        with open(file_name, "w") as f:
            f.write(yaml_text)

    def get_deployment_options(self):  
        options = {}

        deployment_name = input("Enter name for deployment (Press Enter for default): ")
        if deployment_name:
            options["deploymentName"] = deployment_name

        deployment_namespace = input("Enter name for deployment namespace (Press Enter for default): ")
        if deployment_namespace:
            options["deploymentNameSpace"] = deployment_namespace

        replicas = input("Enter replicas for deployment (Press Enter for default): ")
        if replicas:
            options["replicas"] = replicas

        container_name = input("Enter name for the container (Press Enter for default): ")
        if container_name:
            options["containerName"] = container_name

        image = input("Enter container image: ")
        if image:
            options["image"] = image
            
        container_port = input("Enter container port (Press Enter for default): ")
        if container_port:
            options["containerPort"] = container_port

        match_labels_input = input("Enter matchLabels for selector as key:value pairs separated by commas (e.g., app:myapp,version:v1): ")
        if match_labels_input:
            match_labels = {}
            for pair in match_labels_input.split(','):
                key, value = pair.split(':')
                match_labels[key.strip()] = value.strip()
            options["matchLabels"] = match_labels

        return options

    def get_ingress_options(self):  
        options = {}

        ingress_name = input("Enter name for ingress (Press Enter for default): ")
        if ingress_name:
            options["ingressName"] = ingress_name

        ingress_namespace = input("Enter name for ingress namespace (Press Enter for default): ")
        if ingress_namespace:
            options["ingressNameSpace"] = ingress_namespace

        path = input("Enter path for ingress (e.g. /api): ")
        if path:
            options["path"] = path

        path_type = input("Enter path type for ingress (e.g. Prefix, Exact): ")
        if path_type:
            options["pathType"] = path_type

        service_name = input("Enter service name for ingress (Press Enter for default): ")
        if service_name:
            options["serviceName"] = service_name

        port_name = input("Enter port name for the service in ingress (Press Enter for default): ")
        if port_name:
            options["portName"] = port_name

        return options
    
    def get_service_options(self):  
        options = {}

        service_name = input("Enter name for service (Press Enter for default): ")
        if service_name:
            options["serviceName"] = service_name
            
        service_labels_input = input("Enter service labels as key:value pairs separated by commas (e.g., app:myapp,version:v1): ")
        if service_labels_input:
            service_labels = {}
            for pair in service_labels_input.split(','):
                key, value = pair.split(':')
                service_labels[key.strip()] = value.strip()
            options["serviceLabels"] = service_labels

        port_name = input("Enter port name for the service (Press Enter for default): ")
        if port_name:
            options["portName"] = port_name

        port = input("Enter port number for the service: ")
        if port:
            options["port"] = port

        target_port = input("Enter targetPort for the service (Press Enter for default): ")
        if target_port:
            options["targetPort"] = target_port

        return options

    def get_pv_options(self):  
        options = {}

        pv_name = input("Enter name for pv (Press Enter for default): ")
        if pv_name:
            options["pvName"] = pv_name

        storage = input("Enter storage capacity for the pv (e.g., 10Gi): ")
        if storage:
            options["storage"] = storage

        access_modes_input = input("Enter access modes separated by commas (e.g., ReadWriteOnce, ReadOnlyMany, ReadWriteMany): ")
        if access_modes_input:
            access_modes = [mode.strip() for mode in access_modes_input.split(',')]
            options["accessModes"] = access_modes

        reclaim_policy = input("Enter the persistent volume reclaim policy (Available options: Retain, Delete, Recycle): ")
        if reclaim_policy:
            options["persistentVolumeReclaimPolicy"] = reclaim_policy

        storage_class_name = input("Enter the name of the storage class (Press Enter for default): ")
        if storage_class_name:
            options["storageClassName"] = storage_class_name

        host_path = input("Enter the path on the host (e.g., /data/pv001/): ")
        if host_path:
            options["hostPath"] = {"path": host_path}
        return options

    def get_pvc_options(self):  
        options = {}

        pvc_name = input("Enter name for pvc (Press Enter for default): ")
        if pvc_name:
            options["pvcName"] = pvc_name

        pvc_namespace = input("Enter name for pvc namespace (Press Enter for default): ")
        if pvc_namespace:
            options["pvcNameSpace"] = pvc_namespace

        access_modes_input = input("Enter access modes separated by commas (e.g., ReadWriteOnce, ReadOnlyMany, ReadWriteMany): ")
        if access_modes_input:
            access_modes = [mode.strip() for mode in access_modes_input.split(',')]
            options["accessModes"] = access_modes

        storage = input("Enter storage capacity for the pvc (e.g., 10Gi): ")
        if storage:
            options["storage"] = storage

        return options

    def get_hpa_options(self):  
        options = {}

        hpa_name = input("Enter name for HPA (Press Enter for default): ")
        if hpa_name:
            options["hpaName"] = hpa_name
            
        hpa_namespace = input("Enter name for HPA namespace (Press Enter for default): ")
        if hpa_namespace:
            options["hpaNameSpace"] = hpa_namespace

        scale_target_ref_name = input("Enter scaleTargetRef name (e.g., my-deployment): ")
        if scale_target_ref_name:
            options["scaleTargetRefName"] = scale_target_ref_name

        min_replicas = input("Enter the minimum number of replicas: ")
        if min_replicas:
            options["minReplicas"] = int(min_replicas)

        max_replicas = input("Enter the maximum number of replicas: ")
        if max_replicas:
            options["maxReplicas"] = int(max_replicas)

        target_cpu_utilization_percentage = input("Enter target CPU utilization percentage (e.g., 50): ")
        if target_cpu_utilization_percentage:
            options["targetCPUUtilizationPercentage"] = int(target_cpu_utilization_percentage)

        return options

    def run(self):  
        input_number = int(input("Enter a number (1: Deployment, 2: Ingress, 3: Service, 4: PV, 5: PVC, 6: HPA): ")) 


    def run(self):  
        input_number = int(input("Enter a number (1: Deployment, 2: Ingress, 3: Service, 4: PV, 5: PVC, 6: HPA): "))

        kind_map = {
            1: "deployment",
            2: "ingress",
            3: "service",
            4: "pv",
            5: "pvc",
            6: "HorizontalPodAutoscaler"
        }

        options_map = {
            1: self.get_deployment_options,
            2: self.get_ingress_options,
            3: self.get_service_options,
            4: self.get_pv_options,
            5: self.get_pvc_options,
            6: self.get_hpa_options
        }

        kind = kind_map.get(input_number)

        options = {}
        if kind:
            options = options_map.get(input_number)()

            prompt = self.create_query(kind, options)  
            response = self.get_chat_response(prompt)  
            yaml_text = self.extract_yaml(response)  
            if yaml_text:
                print(yaml_text)

                file_name = f"{kind}_generated.yaml"
                self.save_yaml_file(yaml_text, file_name)  
                print(f"{file_name}에 저장되었습니다.")
            else:
                print("YAML 텍스트를 찾을 수 없습니다.") 
        else:
            print("올바른 숫자를 입력하세요.")

if __name__ == "__main__":
    creator = KubernetesYAMLCreator()  
    creator.run()  